import colors from 'vuetify/es5/util/colors'
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: "Grant Sander",
    title: "Grant Sander",
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.6.3/css/all.css' },
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/pwa',
  ],

  manifest: {
    name: "Grant Sander",
    short_name: "GKS",
    lang: "en",
    display: "standalone",
    background_color: "#ffffff",
    description: "Grant Sander's Personal Homepage",
    theme_color: "#395f30",
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: [/^vuetify/],
    parallel: true,
    babel: {
      plugins: [
        ['transform-imports', {
          'vuetify': {
            'transform': 'vuetify/es5/components/${member}',
            'preventFullImport': true
          }
        }]
      ]
    },

    plugins: [
      new VuetifyLoaderPlugin()
    ]
  }
}
